'use strict';
var webdriver = require('browserstack-webdriver');
var b = require('./browsers.js');
var async = require('async');

b(function (browsers)
{
// Input capabilities
	var success = 0;
	var failure = 0;
	var total = browsers.length;
	var queue = async.queue(runDriver, 2);
	function makeDriver(capabilities)
	{
		capabilities['browserstack.user'] = 'glenblosser';
		capabilities['browserstack.key'] = 'kbUayu5Ejfvc5vFHPq8Z';

		return new webdriver.Builder()
			.usingServer('http://hub.browserstack.com/wd/hub')
			.withCapabilities(capabilities).build();
	}
	function runDriver(browser, cb)
	{
		var driver = makeDriver(browser);
		driver.get('http://www.google.com/ncr');
		driver.findElement(webdriver.By.name('q')).sendKeys('BrowserStack');
		driver.findElement(webdriver.By.name('btnG')).click();

		driver.getTitle().then(function (title) {
			if (title.toLowerCase().indexOf('google') > -1) {
				success = success + 1;
			}
			else {
				failure = failure + 1;
			}
			var perc = Math.round(((success + failure) / total) * 100);
			if (perc < 10) {
				perc = '0' + perc;
			}
			perc = perc + '%';
			write(perc + ' complete | Successes: ' + success + ' | Failures: ' + failure);
		});
		driver.wait(cb, 1000);
		driver.quit();
	}
	queue.pause();
	browsers.forEach(launchTestsForDesiredBrowser);
	queue.resume();

	function launchTestsForDesiredBrowser(browser) {
		queue.push(browser);
	}
});

function write(mess)
{
	process.stdout.write('\r' + mess);
}